import { Component, OnInit } from '@angular/core';
import {Category} from './category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  inputs:['category']
})
export class CategoryComponent implements OnInit {

  category:Category;

  constructor() { }

  ngOnInit() {
  }

} 
