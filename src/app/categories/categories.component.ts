import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../products/products.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

   categories;

  constructor(private _productsService:ProductsService) { }

  ngOnInit() {
       this._productsService.getCategories().subscribe(productData => {
       this.categories = productData;
   });
  }

}
