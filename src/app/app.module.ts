import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import {AngularFireModule} from 'angularfire2';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './products/product/product.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './categories/category/category.component';

export const firebaseConfig = {
    apiKey: "AIzaSyC72utERGVLfxVU6sj5uTx94izx7B9sOS8",
    authDomain: "exam-example.firebaseapp.com",
    databaseURL: "https://exam-example.firebaseio.com",
    storageBucket: "exam-example.appspot.com",
    messagingSenderId: "686042635380"
}

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductComponent,
    CategoriesComponent,
    CategoryComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
