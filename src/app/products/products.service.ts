import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {AngularFire} from 'angularfire2';

@Injectable()
export class ProductsService {

  productsObservable;

  getProducts(){
    return this.productsObservable = this.af.database.list('/product');
   }

   getCategories(){
     this.productsObservable = this.af.database.list('/product').map(
     products =>{
       products.map(
         product => {
           product.catTitles = [];
           product.catTitles.push(this.af.database.object('/category/'+product.category));
         }
       );
       return products;
     }
   );
   return this.productsObservable;
   }

   deleteProduct(product){
    this.af.database.object('/product/'+product.$key).remove(); 
  }

  constructor(private af:AngularFire) { }

}
