import { ExamExamplePage } from './app.po';

describe('exam-example App', function() {
  let page: ExamExamplePage;

  beforeEach(() => {
    page = new ExamExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
